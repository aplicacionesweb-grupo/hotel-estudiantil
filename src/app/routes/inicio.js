const dbConnection = require('../../config/dbConection');


// Connect with a connection pool.

module.exports = app => {

    const connection = dbConnection();

    app.get('/', (req, res) => {
       
        res.render('inicio.ejs');
       
    });


    app.get('/reservacion', (req, res) => {
        res.render('reservacion.ejs');
    });

    app.get('/registrados', (req, res) => {
       /* connection.query('SELECT * FROM registro', (err, result) => {
            res.render('registrados', {
              news: result
            });
          });*/
          res.render('registrados.ejs');
    });


    app.get('/quienes-somos', (req, res) => {
        res.render('somos.ejs');
    });
}



