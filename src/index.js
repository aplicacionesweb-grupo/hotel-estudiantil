const app = require('./config/server');

require('./app/routes/inicio')(app);


// iniciar servidor 

app.listen(app.get('port'), () => {
    console.log('Aplicación ejemplo, escuchando el puerto ' , app.get('port'));
  });