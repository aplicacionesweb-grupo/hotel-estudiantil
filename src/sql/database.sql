CREATE DATABASE datahotel;

USE news_portal;

SHOW TABLES;

CREATE TABLE cliente (
  id_news INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(100),
  news TEXT,
  data_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

DESCRIBE cliente;

INSERT INTO cliente (title, news) values ('my title', 'content of the news');

SELECT * FROM cliente;